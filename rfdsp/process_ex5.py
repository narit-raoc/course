import numpy as np
import rfutil

fs = 100e3  # Sample rate [samples/second]

# -----------------------------------------------------------------------------
# Parameters to analyze signals.
# EDIT the variables xlabel, ylabel, Ts, Ycal to present a graph of
# Voltage [V] vs. Time [s]
# -----------------------------------------------------------------------------
filename_in = "ex5.iq"  # Name of file to read
xlabel = "sample number"
ylabel = "amplitude"
Ts = 1 / 1  # Sample period [seconds]
Ycal = 1e-2  # watts per 1.0 amplitude of digital data

# ==================== DOT NOT EDIT BELOW THIS LINE ===========================
# Read entire file
file_in = open(filename_in, "rb")
dtype_iq_pair = np.dtype([("real", np.float32), ("imag", np.float32)])
data = np.fromfile(file_in, dtype=dtype_iq_pair)
sig = data["real"] + 1j * data["imag"]
print("read file: name=%s, array length=%d" % (filename_in, sig.shape[0]))
file_in.close()

# Create graphs
rfutil.analyze_spectrum(fs, Ycal, sig)
