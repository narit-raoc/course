import numpy as np
import rfutil

fs = 100e3  # Sample rate [samples/second]
filename_out = "ex7.iq"

# -----------------------------------------------------------------------------
# Parameters to analyze signals.
# EDIT the variables xlabel, ylabel, Ts, Ycal to present a graph of
# Voltage [V] vs. Time [s]
# -----------------------------------------------------------------------------
xlabel = "sample number"
ylabel = "amplitude"
Ts = 1 / 1  # Sample period [seconds]
Ycal = 1e-2  # watts per 1.0 amplitude of digital data

# ==================== DOT NOT EDIT BELOW THIS LINE ===========================
# Parameters to generate signals
length_noise = 6.0  # Length of background noise [seconds]
length_signal = length_noise  # Length of signal [seconds]
delay_signal = 0  # Delay to start signal in noise buffer [seconds]
freq = 25e3  # Frequency of signal 1 [Hz]
S = 1  # Amplitude of signal noise [no unit]
N = 1e-3  # Amplitude of noise [no unit]

# Doppler fly-by signal parameters
velocity = rfutil.speed_of_light / 10

# Generate background noise
noise = N * rfutil.siggen_noise(fs, length_noise)


sig_doppler = S * rfutil.siggen_doppler_flyby(
    fs, length_signal, freq, velocity, debug=False
)

# Add it to the signal accumulator buffer with desired delay
sig = rfutil.accumulate(fs, delay_signal, noise, sig_doppler)

# Write file
rfutil.write_iq_file(filename_out, sig)
