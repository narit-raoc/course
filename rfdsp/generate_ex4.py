import numpy as np
import rfutil

fs = 10e3  # Sample rate [samples/second]
filename_out = "ex4.iq"

# ==================== DOT NOT EDIT BELOW THIS LINE ===========================
# Parameters to generate signals
length_noise = 0.001  # Length of background noise [seconds]
length_signal = length_noise  # Length of signal [seconds]
delay_signal = 0  # Delay to start signal in noise buffer [seconds]
freq = 1200  # Frequency of signal [Hz]
S = 1  # Amplitude of signal noise [no unit]
N = 0  # Amplitude of noise [no unit]

# Generate background noise
noise = N * rfutil.siggen_noise(fs, length_noise)

# Generate a complex-valued cosine signal (spectral line)
sig_cosine = S * rfutil.siggen_cosine(fs, length_signal, freq, window=False)

# Add it to the signal accumulator buffer with desired delay
sig = rfutil.accumulate(fs, delay_signal, noise, sig_cosine)

# Write file
rfutil.write_iq_file(filename_out, sig)
