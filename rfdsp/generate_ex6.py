import numpy as np
import rfutil

fs = 100e3  # Sample rate [samples/second]
filename_out = "ex6.iq"

# -----------------------------------------------------------------------------
# Parameters to analyze signals.
# EDIT the variables xlabel, ylabel, Ts, Ycal to present a graph of
# Voltage [V] vs. Time [s]
# -----------------------------------------------------------------------------
xlabel = "sample number"
ylabel = "amplitude"
Ts = 1 / 1  # Sample period [seconds]
Ycal = 1e-2  # watts per 1.0 amplitude of digital data

# ==================== DOT NOT EDIT BELOW THIS LINE ===========================
# Parameters to generate signals
length_noise = 20.0  # Length of background noise [seconds]
length_signal = length_noise  # Length of signal [seconds]
delay_signal = 0  # Delay to start signal in noise buffer [seconds]
freq1 = -20000  # Frequency of signal 1 [Hz]
freq2 = 30000  # Frequency of signal 2 [Hz]
S = 1  # Amplitude of signal noise [no unit]
N = 1e-3  # Amplitude of noise [no unit]

# BPSK signal parameters
symbol_rate = 9600

# Generate background noise
noise = N * rfutil.siggen_noise(fs, length_noise)

# Generate a complex-valued cosine signal (spectral line)
sig_cosine = S * rfutil.siggen_cosine(fs, length_signal, freq1, window=False)

# Generate a BPSK data communication signal of random data
sig_bpsk = S * rfutil.siggen_bpsk(fs, length_noise / 4, freq2, symbol_rate, debug=False)

# Add it to the signal accumulator buffer with desired delay
sig = rfutil.accumulate(fs, delay_signal, noise, sig_cosine)
sig = rfutil.accumulate(fs, length_noise / 4, sig, sig_bpsk)
sig = rfutil.accumulate(fs, length_noise * 3 / 4, sig, sig_bpsk)

# Write file
rfutil.write_iq_file(filename_out, sig)
