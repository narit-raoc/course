import numpy as np
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.style.use('dark_background')
import rfutil
fs = 100e3  						# Sample rate [samples/second]
channel_count = 1000

# ==================== DOT NOT EDIT BELOW THIS LINE ===========================
# Parameters to generate signals
length_noise = 1.0				# Length of background noise [seconds]
length_signal = length_noise	# Length of signal [seconds]
delay_signal = 0				# Delay to start signal in noise buffer [seconds]
freq1 = 10023					# Frequency of signal 1 [Hz]
S = 1.0							# Amplitude of signal noise [no unit]
N = 1							# Amplitude of noise [no unit]

# Generate background noise
noise = N*rfutil.siggen_noise(fs, length_noise)

# Generate a complex-valued cosine signal (spectral line)
sig_cosine = S*rfutil.siggen_cosine(fs, length_signal, freq1, window=False)

# Add it to the signal accumulator buffer with desired delay
sig = rfutil.accumulate(fs, delay_signal, noise, sig_cosine)

# Channelize data
data_cube = rfutil.channelize_fft(sig, channel_count)

# Select the first acquisition to show data before averaging
spectrum_no_average = 20*np.log10(np.abs(data_cube[0,:]))

# Calculate the RMS POWER AVERAGE of the data set across all time steps
spectrum_power_average = np.mean(20*np.log10(np.abs(data_cube)), axis=0)

# Calculate the COHERENT VECTOR AVERAGE of the data set across all time steps
spectrum_vector_average = 20*np.log10(np.abs(np.mean(data_cube, axis=0)))


# Generate 3 plots
ax1 = plt.subplot(1,1,1)
#ax1.plot(spectrum_no_average,'y-', label='no average')
ax1.plot(spectrum_power_average,'c-', label='RMS Power average')
ax1.plot(spectrum_vector_average,'m-', label='vector average')
ax1.set_title('Compare RMS Power vs. Vector-Averaged Spectrum [N_avg = %d]' % data_cube.shape[0])
ax1.set_xlabel('FFT channel index')
ax1.set_ylabel('Magnitude [dB]')
ax1.grid(True, which='both')
ax1.legend()


plt.show()