import sys
import numpy as np
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.pyplot as plt
plt.style.use('dark_background')

class CombGenerator():
	def __init__(self, sample_rate, pulse_width_s, pulse_repetition_period_s, n_pulses):
		'''
		Parameters
		----------
		pulse_width_s : width of each pulse in the comb function.  In theory, we would like width=0,
		but real electronic circuit cannot generate zero width pulse

		pulse_repetition_period_s : pulse repetition period.  time period at which narrow pulse is transmitted from the
		comb generator. (1 / pulse repetition frequency PRF)

		n_pulses: total number of pulses to generate for one calibration measurment (and calculate 1 spectrum)
		'''

		self.sample_rate = sample_rate
		self.pulse_width_s = pulse_width_s
		self.pulse_repetition_period_s = pulse_repetition_period_s
		self.n_pulses = n_pulses


		sample_period = 1 / sample_rate
		
		total_length_sec = n_pulses * pulse_repetition_period_s
		total_length_samples = int(total_length_sec / sample_period)

		# Generate time axis data
		self.tt_s = np.linspace(0, total_length_sec, total_length_samples)
		self.ff_hz = np.linspace(0, sample_rate, total_length_samples)

		# Generate 1 pulse period
		period_length_samples = pulse_repetition_period_s / sample_period
		
		nsamples_high = int(pulse_width_s / sample_period)
		# If we want pulse width < sample_period, we must give 1 sample to the pulse.
		if(nsamples_high == 0):
			print('pulse width < sample_period.  create 1 sample minimum pulse width')
			nsamples_high = 1

		nsamples_low = int(period_length_samples - nsamples_high)

		print('sample period: {}, pulse_repetition_period: {}'.format(sample_period, pulse_repetition_period_s))
		print('sample_rate: {}'.format(sample_rate))
		print('total_length_sec: {}, total_length_samples: {}'.format(total_length_sec, total_length_samples))
		print('single pulse nsamples_high: {}, single pulse nsamples_low: {}'.format(nsamples_high, nsamples_low))

		pulse_high = np.ones(nsamples_high)
		pulse_low = np.zeros(nsamples_low)
		
		self.single_pulse = np.concatenate([pulse_high, pulse_low])
		self.pulse_train = np.tile(self.single_pulse, n_pulses)
		print('pulse train raw data {}'.format(repr(self.pulse_train)))
		self.spectrum = np.fft.fft(self.pulse_train)


class Channel:
	def __init__(self, length_m):
		self.length_m = length_m

	def work(self, signal_in):
		return signal_in

if __name__ == "__main__":
	# Scale all the numbers by 1E9 to make the processing faster and not crash
	# We multiply / divide results and plot axes to interpret the
	# physical result from the scaled computation.
	scale_factor = 1e9
	
	# actual pulse width is 50 ps.  50e-12 * 1E9 = 50e-3
	pulse_width_actual = 50e-12
	pulse_width_scaled = pulse_width_actual * scale_factor
	
	# actual sample rate I want for this sim is 1000 GHz to have 1 picosecond
	# time resolution
	sample_period_actual = 1e-12
	sample_rate_actual = 1/sample_period_actual
	sample_rate_scaled = sample_rate_actual / scale_factor

	# actual pulse repetition rate is 1 / 5 MHz = 200ns.  Use 200e-9 * 1e9  = 200
	prf_actual = 5e6
	prp_actual = 1/prf_actual
	prp_scaled = prp_actual * scale_factor
	
	n_pulses = 2**3

	combgen = CombGenerator(sample_rate_scaled, pulse_width_scaled, prp_scaled, n_pulses)

	# Constants to convert units for report
	NANOSEC_PER_S = 1E9
	PICOSEC_PER_SEC = 1E12
	MEGAHERZ_PER_HZ = 1E6
	GIGAHERZ_PER_HZ = 1E9

	fig1 = plt.figure("Comb Genreator", figsize=(8,10))
	ax1a = plt.subplot(3, 1, 1)
	ax1a.grid(which='both', alpha=0.4)
	ax1a.set_title('Sample Period: {:.1f} [ps], Pulse Width: {:.1f} [ps], PRF: {} [MHz], N_pulses: {}'.format(sample_period_actual*PICOSEC_PER_SEC, pulse_width_actual*PICOSEC_PER_SEC, prf_actual/MEGAHERZ_PER_HZ, n_pulses))
	ax1a.set_xlabel('Time [ns]')
	ax1a.set_ylabel('Voltage [V]')
	ax1a.plot(NANOSEC_PER_S * combgen.tt_s / scale_factor, combgen.pulse_train, color='yellow')

	spectrum_magnitude = np.abs(combgen.spectrum)
	spectrum_normalized = spectrum_magnitude / np.max(spectrum_magnitude)
	spectrum_dB = 20*np.log10(spectrum_normalized+1e-3)

	spectrum_phase = np.angle(combgen.spectrum)

	ax2a = plt.subplot(3, 1, 2)
	ax2a.grid(which='both', alpha=0.4)
	#ax2a.set_xlabel('Frequency [GHz]')
	ax2a.set_ylabel('Spectrum Magnitude [dB]')
	ax2a.plot(combgen.ff_hz * scale_factor / GIGAHERZ_PER_HZ, spectrum_dB, color='blue', linestyle='-')
	ax2a.plot(combgen.ff_hz * scale_factor / GIGAHERZ_PER_HZ, spectrum_dB, color='red', marker='+', linestyle='')
	ax2a.set_xlim((0, 40))
	ax2a.set_ylim((-60, 10))

	ax3a = plt.subplot(3, 1, 3)
	ax3a.grid(which='both', alpha=0.4)
	ax3a.set_xlabel('Frequency [GHz]')
	ax3a.set_ylabel('Spectrum Phase [rad]')
	ax3a.plot(combgen.ff_hz * scale_factor / GIGAHERZ_PER_HZ, spectrum_phase, color='blue', linestyle='-')
	ax3a.plot(combgen.ff_hz * scale_factor / GIGAHERZ_PER_HZ, spectrum_phase, color='red', marker='+', linestyle='')
	ax3a.set_xlim((0, 40))
	#ax2a.set_ylim((-50, 0)

	plt.show()